/*
 * @Descripttion: 应用主文件
 * @version: 1.0.0
 * @Author: xuecheng
 * @Date: 2022-03-25 09:45:41
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-04-07 17:41:08
 */
const express = require('express')
const bodyParser = require('body-parser')    //解析post请求body，往req上添加body属性
const session = require('express-session')         //session插件
const MongoStore = require('connect-mongo')       //session持久化插件
 

const cors = require('./config/cors')                
const {APP_PORT,SECRET,MONGOOSEURL} = require('./config/index')
const Router = require('./router/index')
const {connect,_getModel} = require('./db/index')


const app = express();

cors(app)   //设置跨域

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))


app.use(session({
    secret: SECRET,      //加密串
    name: 'sessionid',
    resave: false,
    rolling: true,   //每次请求重置cookie过期时间
    cookie: {
        path: '/',
        httpOnly: true,
        maxAge: 24 * 60 * 60 * 1000 * 3         //必须为数字类型
    },
    store: MongoStore.create({
        mongoUrl: MONGOOSEURL,
        touchAfter: 24 * 60 * 60       //一天才会更新一次数据库session,  秒 为单位
    })
}))





//链接数据库
connect();
global._getModel = _getModel;

app.use(express.static('static')) //设置静态资源目录

Router(app);


app.listen(APP_PORT,() => {
    console.log(`server is running on http://localhost${APP_PORT}`);
    console.log(
   "  __    __   ______  ________  ________        _______    ______    ______   __    __     \n"+
   " |  \  |  \ /      \|        \|        \      |       \  /      \  /      \ |  \  /  \    \n"+
   " | $$\ | $$|  $$$$$$\\$$$$$$$$| $$$$$$$$      | $$$$$$$\|  $$$$$$\|  $$$$$$\|   $$     /$$     \n"+
   " | $$$\| $$| $$  | $$  | $$   | $$__         | $$__/ $$| $$  | $$|$$  | $$| $$/   $$     \n"+
   " | $$$$\ $$| $$  | $$  | $$   | $$  \         | $$    $$| $$  | $$|$$  | $$| $$  $$      \n"+
   " | $$\$$ $$| $$  | $$  | $$   | $$$$$        | $$$$$$$\|  $$  | $$|$$  | $$| $$$$$\      \n"+
   " | $$ \$$$$| $$__/ $$  | $$   | $$_____      | $$__/ $$| $$__/ $$|$$__/ $$| $$ \$$\     \n"+
   " | $$  \$$$  \$$    $$  | $$   | $$     \      | $$    $$  \$$   $$  \$$   $$|  $$  \$$\    \n"+
   "   \$$   \$$  \$$$$$$      \$$    \$$$$$$$$        \$$$$$$$   \$$$$$$   \$$$$$$    \$$    \$$    \n"
    );
})
