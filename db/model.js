/*
 * @Descripttion: 数据模型
 * @version: 1.0.0
 * @Author: xuecheng
 * @Date: 2022-03-25 16:29:47
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-04-19 16:14:43
 */
// import mongoose from "mongoose"

module.exports = {
    //用户信息模板
    userInfo: {
        userName: {type: String,required: true},
        password: {type: String, required: true},
        email: {type: String, required: true},
        age: {type: Number,default:0},
        sex: {type: String,enum:['男','女','保密'],default:'保密'},
        introduce: {type: String,maxlength:200},
        avatar:{type:String,default:'/default_img/avatar.png'},
        isDelete: {type: Number, default: 0},     //是否注销  0 否    1 是
        createTime: {type: Date, default: Date.now() + 28800000 + ''}
    },
    //计划信息模板
    planContent: {
        // userId: [{type: mongoose.Schema.Types.ObjectId, ref: 'userInfo', required: true}],
        userId: {type: String, required: true},
        content: {type: String, required: true},            //内容
        level: {type: Number, enum: [1,2,3,4],default: 1},   //12 普通计划   34重要计划
        state: {type: Number, enum: [1,2,3], default: 1},      //1 待完成   2  已完成   3 已逾期
        noteTip: {type: String, required: false},            //备注
        createTime: {type: Date, default: Date.now() + 28800000 + ''},
        completeTime: {type: Date, required: true}
    },
    //笔记信息模板
    myNote: {
        // userId: [{type: mongoose.Schema.Types.ObjectId, ref: 'userInfo', required: true}],
        userId: {type: String, required: true},
        main: {type: String, required: true},
        mainDelta: {type: String, required: true},
        tag: {type: String, required: false},  // 标签
        level: {type: Number, enum: [1,2,3,4],default: 1},    //等级
        createTime: {type: Date, default: Date.now() + 28800000 + ''}
    }
}