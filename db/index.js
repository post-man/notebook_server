/*
 * @Descripttion: 数据库模块
 * @version: 1.0.0
 * @Author: xuecheng
 * @Date: 2022-03-25 16:19:55
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-03-29 10:06:55
 */
const mongoose = require('mongoose')
const {MONGOOSEURL} = require("../config/index")
const Model = require('./model')

for(let i in Model){
    mongoose.model(i,new mongoose.Schema(Model[i]));
}


module.exports = {
    connect: function(){
        mongoose.connect(MONGOOSEURL,(err) => {
            if(err) return console.log("数据库链接失败。");

            console.log("数据库已连接！！！！！");
        })
    },
    _getModel: function(type){
        return mongoose.model(type);
    }
}