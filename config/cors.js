/*
 * @Descripttion: 跨域设置
 * @version: 1.0.0
 * @Author: xuecheng
 * @Date: 2022-03-25 13:56:15
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-04-02 17:17:59
 */

const cors = require('cors')

module.exports = app => {
    const corsOptions = {
        origin:'http://124.223.82.82',              //设置允许请求的域名
        optionsSuccessStatus: 200,
        credentials: true,
        methods: ['GET','PUT','POST']
    }

    app.use(cors(corsOptions))


    //不使用插件设置跨域
    // app.all('*',(req,res,next) => {
    //     res.header("Access-Control-Allow-Origin", "http://localhost:3000");
    //     res.header("Access-Control-Allow-Headers", "Content-Type,Content-Length, Authorization, Accept,X-Requested-With");
    //     res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
    //     res.header('Access-Control-Allow-Credentials','true');
    //     if(req.method=="OPTIONS") res.send(200);/*让options请求快速返回*/
    //     else  next();
    // })

}