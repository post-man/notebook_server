/*
 * @Descripttion: 读取env文件
 * @version: 1.0.0
 * @Author: xuecheng
 * @Date: 2022-03-25 09:53:48
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-03-25 09:56:08
 */
const dotenv = require('dotenv')
//将.env文件的配置项读取到process.env中
dotenv.config();

module.exports = process.env
