/*
 * @Descripttion: 鉴权公共模块
 * @version: 1.0.0
 * @Author: xuecheng
 * @Date: 2022-04-01 16:13:03
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-04-07 16:46:21
 */

const crypto = require('crypto')

//密码加密中间件   -----------使用node自带crypto算法加密
function encryptionPassowrd(req,res,next){
    let {password} = req.body;
    let result = crypto.createHash('md5').update(password).digest('hex');
    req.body.password = result;
    console.log("密码加密")
    next();    
}

//密码加密函数
function encryPassword (password) {
    const res = crypto.createHash('md5').update(password).digest('hex');
    return res
}


//登录权限校验
function isLogin(req,res,next){
    if(!req.session.userInfo){
        res.status(401);       //未登录状态码
        res.send({
            code:'0001',
            msg:'未登录，请先登录再访问'
        });
        return;
    }
    next();
}




module.exports = {
    encryptionPassowrd,
    isLogin,
    encryPassword
}