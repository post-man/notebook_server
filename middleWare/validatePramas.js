/*
 * @Descripttion:校验参数中间件
 * @version: 1.0.0
 * @Author: xuecheng
 * @Date: 2022-03-25 15:03:03
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-04-08 16:50:55
 */
//TODO    使用express-validator插件校验参数
const {isEmail} = require("../utils/regExp")


function validateRegister (req,res,next) {
    let {userName,password,passwordTwo,email,emailCode} = req.body;
    console.log(userName,password,passwordTwo,email,emailCode)
    if(!userName || !password || !passwordTwo || !email || !emailCode){
        res.send({
            code:'1000',
            msg:'参数缺失，请确认'
        });
        return;
    }
    if(password.length < 6 || password.length > 12){
        res.send({
            code: '0001',
            msg:'密码长度错误'
        });
        return;
    }
    next();
}

function validateEmail (req,res,next) {
    let {email} = req.query;
    if(!email){
        res.send({
            code:'1000',
            msg:'参数缺失，请确认'
        })
        return;
    }
    if(!isEmail(email)){
        res.send({
            code:'0001',
            msg:'邮箱格式不正确，请确认'
        });
        return;
    }
    next();
}

function validateUserName (req,res,next) {
    let {userName} = req.query;
    if(userName){
        next();
        return;
    }
    res.send({
        code:'1000',
        msg:'参数缺失，请确认'
    })
}

function validateLogin (req,res,next) {
    let {userName,password} = req.body;
    console.log("接收登录参数",req.body);
    if(userName && password){
        next();
        return;
    }
    res.send({
        code:'1000',
        msg:'参数缺失，请确认'
    })
}

function validateAddPlan(req,res,next){
    let {level,content,completeTime} = req.body;
    if(level && content && completeTime){
        next();  
        return;
    }
    res.send({
        code:'1000',
        msg:'参数缺失，请确认'
    })
}

module.exports = {
    validateRegister,
    validateEmail,
    validateUserName,
    validateLogin,
    validateAddPlan
}