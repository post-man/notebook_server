/*
 * @Descripttion: 计划service层
 * @version: 1.0.0
 * @Author: xuecheng
 * @Date: 2022-04-08 17:12:19
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-04-19 17:33:29
 */


//添加计划
async function addPlanService(plan){
    const res = await global._getModel('planContent').create(plan);
    console.log("添加计划",res);
    return res ? res : '01';
}

//获取待完成计划
async function getAllPlanService(pageMsg){
    let {userId,currentPage,pageSize} = pageMsg;
    let now = Date.now()
    const res  = await global._getModel('planContent')
        .find({
            userId,
            state: 1,
            completeTime: {$gte: now}
        }).skip((currentPage - 1)*parseInt(pageSize)).limit(parseInt(pageSize));
    const count = await global._getModel('planContent').find({userId, state: 1, completeTime: {$gte: now}}).count();
    return {
        res,
        count
    }
}

async function getTodayPlanService (userId) {
    const start = new Date(new Date().toLocaleDateString()).getTime();
    const end = new Date(new Date(new Date().toLocaleDateString()).getTime() + 24*60*60*1000 + 8*60*60*1000 ).getTime();
    const res = await global._getModel('planContent').find({$and: [
            {userId},
            {completeTime: {$gte: start}},
            {completeTime: {$lte: end}}
        ]});
    return res;
}

//删除计划
async function deletePlanService(planId){
    let res = await global._getModel('planContent').findByIdAndDelete(planId);
    return res ? res : '01'
}


async function updatePlanService(planId,plan){
    let res = await global._getModel('planContent').findByIdAndUpdate(planId,plan);
    console.log('更新数据', res)
    return res ? res : '01'
}

async function searchPlanService (paramsObj) {
    const { currentPage, pageSize} = paramsObj
    let searchObj = {}
    paramsObj.level && Object.assign(searchObj, {leval: paramsObj.level})
    paramsObj.state && Object.assign(searchObj, {state: paramsObj.state})
    let searchContent = {}
    if (paramsObj.searchWords) {
        searchContent = {content: {$regex: paramsObj.searchWords}}
    }
    const res = await global._getModel('planContent').find({
        $or: [searchContent, searchObj]
    }).skip((currentPage - 1)*parseInt(pageSize)).limit(parseInt(pageSize));
    const count = await global._getModel('planContent').find({ $or: [searchContent, searchObj] }).count();
    return {
        res,
        count
    }
}

module.exports = {
    addPlanService,
    getAllPlanService,
    deletePlanService,
    updatePlanService,
    searchPlanService,
    getTodayPlanService
}