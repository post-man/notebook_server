/*
 * @Descripttion: 鉴权service层
 * @version: 1.0.0
 * @Author: xuecheng
 * @Date: 2022-03-29 11:42:05
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-04-19 17:31:45
 */

async function findUser(obj){
    console.log("查询",obj);
    const res = await global._getModel('userInfo').findOne(obj);
    console.log("查询数据库",res);
    return res ? res : '01';
}

async function addUser(obj){
    const res = await global._getModel('userInfo').create(obj);
    console.log("添加用户",res);
    return res ? res : '01';
}


module.exports = {
    findUser,
    addUser
}