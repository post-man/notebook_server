

async function getPlanAchivedService(pageMsg){
    let {userId,currentPage,pageSize} = pageMsg;
    const res  = await global._getModel('planContent')
        .find({
            userId,
            state: 2
        }).skip((currentPage - 1)*parseInt(pageSize)).limit(parseInt(pageSize));
    const count = await global._getModel('planContent').find({userId, state: 2}).count();
    return {
        res,
        count
    }
}

async function getPlanNoAchivedService(pageMsg){
    let {userId,currentPage,pageSize} = pageMsg;
    let now = Date.now()
    const res  = await global._getModel('planContent')
        .find({
            userId,
            completeTime: { $lt: now }
        }).skip((currentPage - 1)*parseInt(pageSize)).limit(parseInt(pageSize));
    const count = await global._getModel('planContent').find({userId, completeTime: { $lt: now }}).count();
    return {
        res,
        count
    }
}

module.exports = {
    getPlanAchivedService,
    getPlanNoAchivedService
}