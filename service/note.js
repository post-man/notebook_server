

async function getNotesService (pageMsg) {
    const { userId, currentPage, pageSize } = pageMsg
    const res = await global._getModel('myNote').find({userId}).skip((currentPage - 1) * parseInt(pageSize)).limit(parseInt(pageSize))
    const count = await global._getModel('myNote').find({userId}).count()
    return {
        res,
        count
    }
}

async function addNotesService (obj) {
    let time = Date.now() + 28800000 + ''
    console.log('添加笔记时间', time)
    obj = Object.assign({createTime: time}, obj)
    const res = await global._getModel('myNote').create(obj)
    return res ? res : '01'
}

async function updateNotesService (obj) {
    let {noteId, ...main} = obj
    let time = Date.now() + 28800000 + ''
    main = Object.assign({createTime: time}, main)
    const res = await global._getModel('myNote').findByIdAndUpdate(noteId, main)
    return res ? res : '01'
}

async function deleteNotesService (noteId) {
    const res = await global._getModel('myNote').findByIdAndDelete(noteId)
    return res ? res : '01'
}

module.exports = {
    getNotesService,
    addNotesService,
    deleteNotesService,
    updateNotesService
}