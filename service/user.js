async function getUserMsgService (userId) {
    const res = global._getModel('userInfo').findOne({_id: userId})
    return res ? res : '01'
}


async function updateMsgService (userId, obj) {
    const res = global._getModel('userInfo').findByIdAndUpdate(userId, obj)
    return res ? res : '01'
}

module.exports = {
    getUserMsgService,
    updateMsgService
}