/*
 * @Descripttion: 登录模块
 * @version: 1.0.0
 * @Author: xuecheng
 * @Date: 2022-03-25 15:58:57
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-04-19 17:04:48
 */
const {findUser,addUser} = require('../service/auth')
const {sendEmailCodes} = require("../utils/sendEmailCode")

async function checkUserName(req,res){
    const {userName} = req.query;
    if(await findUser({userName}) !== '01'){
        res.send({
            code: '0001',
            msg:'用户名已存在'
        });
        return;
    }
    res.send({
        code:'0000',
        msg:'校验通过'
    });
}


async function registerController(req,res){
    let {userName,password,email} = req.body;
    let obj = {userName,password,email};
    const emailCode = req.body.emailCode;
    if(req.session.emailCode !== emailCode){
        res.send({
            code: '0001',
            msg:'邮箱验证码不正确'
        });
        return;
    }
    let time = Date.now() + 28800000 + ''
    obj = Object.assign({createTime: time}, obj)
    let result = await addUser(obj);
    if(result !== '01'){
        res.send({
            code: '0000',
            msg:'注册成功'
        });
    } else {
        res.send({
            code: '0001',
            msg:'注册失败，请稍候重试'
        });
    }
}

async function sendEmailCode(req,res){
    let emial = req.query.email;
    const code = Date.now().toString().slice(-6);
    let result = await sendEmailCodes(emial,code);
    if(result == '01'){
        res.send({
            code: '0001',
            msg: '邮箱验证码发送失败,不好意思！'
        });
        return;
    }
    req.session.emailCode = code;
    console.log("邮箱验证码",code);
    timer && clearTimeout(timer);
    var timer = setTimeout(() => {
        req.session.emailCode = null;
    },300000);
    res.send({
        code: '0000',
        msg: '邮箱验证码已发送'
    });
}

async function loginController(req,res){
    let user = {
        userName: req.body.userName,
        password: req.body.password
    }
    console.log('登录',user)
    let result = await findUser(user);
    if(result == '01'){
        res.send({
            code: '0001',
            msg:'用户名或密码错误'
        });
        return;
    }
    let {_id,userName,email,age,sex,avatar} = result;
    let userInfo = {
        _id,userName,email,age,sex,avatar
    }
    req.session.userInfo = userInfo;
    res.send({
        code:'0000',
        msg:'登录成功',
        data: userInfo
    });
}


module.exports = {
    checkUserName,
    registerController,
    loginController,
    sendEmailCode
}