const { getNotesService, addNotesService, deleteNotesService, updateNotesService } = require("../service/note")


async function getNotesController(req, res) {
    let userId = req.session.userInfo._id;
    let pageMsg = {
        userId,
        currentPage: req.query.currentPage || 1,
        pageSize: req.query.pageSize || 10
    };
    const result = await getNotesService(pageMsg)
    res.send({
        code: '0000',
        msg: '数据获取成功',
        data: result
    })
}

async function addNotesController (req, res) {
    let userId = req.session.userInfo._id;
    let { main, mainDelta, tag, level } = req.body;
    const paramsObj = {userId, main, mainDelta ,tag, level}
    const result = await addNotesService(paramsObj)
    if (result !== '01') {
        res.send({
            code: '0000',
            msg: '计划添加成功'
        })
    } else {
        res.send({
            code: '0001',
            msg: '计划添加失败，请稍候重试！'
        })
    }
}

async function updateNotesController (req, res) {
    let { noteId, main, mainDelta, tag, level } = req.body;
    const paramsObj  = {noteId, main, mainDelta, tag, level}
    const result = await updateNotesService(paramsObj)
    if (result !== '01') {
        res.send({
            code: '0000',
            msg: '修改成功'
        })
    } else {
        res.send({
            code: '0001',
            msg: '修改失败，请稍候重试！'
        })
    }
}

async function deleteNotesController (req, res) {
    let { noteId } = req.body;
    const result = await deleteNotesService(noteId)
    if (result !== '01') {
        res.send({
            code: '0000',
            msg: '删除成功'
        })
    }
    else {
        res.send({
            code: '0001',
            msg: '删除失败， 请稍候重试！'
        })
    }
}

module.exports = {
    getNotesController,
    addNotesController,
    deleteNotesController,
    updateNotesController
}