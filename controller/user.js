
const { getUserMsgService, updateMsgService } = require('../service/user')
const { encryPassword } = require('../middleWare/authCommon')
async function getUserMsgController (req, res) {
    const userId = req.session.userInfo._id;
    const result = await getUserMsgService(userId)
    if (result === '01') {
        res.send({
            code: '0001',
            msg: '该用户不存在'
        });
        return
    }
    let {password, ...userInfo} = result._doc
    res.send({
        code: '0000',
        msg: '信息查询成功',
        data: userInfo
    })
}

async function updateMsgController (req, res) {
    const userId = req.session.userInfo._id;
    const { email, age, sex, introduce, avatar } = req.body;
    let paramsObj = {}
    email && Object.assign(paramsObj, {email})
    age && Object.assign(paramsObj, {age})
    sex && Object.assign(paramsObj, {sex})
    introduce && Object.assign(paramsObj, {introduce})
    avatar && Object.assign(paramsObj, {avatar})
    const result = await updateMsgService(userId, paramsObj)
    console.log('修改新用户信息', result)
    if (result === '01') {
        res.send({
            code: '0001',
            msg: '信息更新失败'
        });
        return
    }
    res.send({
        code: '0000',
        msg: '信息更新成功'
    })
}

async function resetPasswordController (req, res) {
    const { password, newPassword } = req.body
    const userId = req.session.userInfo._id;
    const user= await getUserMsgService(userId)
    const userPassword = user.password
    console.log('修改密码', userPassword, password)
    if (userPassword !== password) {
        res.send({
            code: '0001',
            msg: '原密码不正确'
        });
        return
    }
    const entryPassword = encryPassword(newPassword)
    const result = await updateMsgService(userId, {password: entryPassword})
    if (result === '01') {
        res.send({
            code: '0002',
            msg: '密码重置失败'
        });
        return
    }
    res.send({
        code: '0000',
        msg: '密码重置成功'
    })
}

module.exports = {
    getUserMsgController,
    updateMsgController,
    resetPasswordController
}