const { getPlanAchivedService, getPlanNoAchivedService } = require('../service/subPlan')
const {updatePlanService} = require("../service/plan");

async function getPlanAchivedController (req, res) {
    let userId = req.session.userInfo._id;
    if(!userId){
        res.send({
            code: '0001',
            msg: "获取计划列表失败，请确认用户Id"
        });
        return;
    }
    let pageMsg = {
        userId,
        currentPage:req.query.currentPage || 1,
        pageSize:req.query.pageSize || 10
    };
    console.log(pageMsg)
    let result = await getPlanAchivedService(pageMsg)
    res.send({
        code: '0000',
        msg: '数据获取成功',
        data: result
    })
}

async function getPlanNoAchivedController (req, res) {
    let userId = req.session.userInfo._id;
    if(!userId){
        res.send({
            code: '0001',
            msg: "获取计划列表失败，请确认用户Id"
        });
        return;
    }
    let pageMsg = {
        userId,
        currentPage:req.query.currentPage || 1,
        pageSize:req.query.pageSize || 10
    };
    console.log(pageMsg)
    let result = await getPlanNoAchivedService(pageMsg)
    res.send({
        code: '0000',
        msg: '数据获取成功',
        data: result
    })
}

async function editPlanController (req, res) {
    let planId = req.body.planId;
    console.log('编辑计划ID',planId);
    if(!planId){
        res.send({
            code: '0001',
            msg: "更新计划失败，请确认计划Id"
        });
        return;
    };
    let {content,level,noteTip,completeTime,state} = req.body;
    let plan = {content,level,noteTip,completeTime,state};
    plan.completeTime = 28800000 + plan.completeTime*1;
    plan.completeTime = plan.completeTime + '';
    let result = await updatePlanService(planId,plan);
    if(result === '01'){
        res.send({
            code: '0002',
            msg: "未查询到该计划数据，请确认"
        });
        return;
    }
    res.send({
        code: '0000',
        msg: "更新计划成功"
    });
}

module.exports = {
    getPlanAchivedController,
    getPlanNoAchivedController,
    editPlanController
}