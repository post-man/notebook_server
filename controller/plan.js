/*
 * @Descripttion: 计划模块
 * @version: 1.0.0
 * @Author: xuecheng
 * @Date: 2022-04-08 16:51:12
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-04-19 17:41:43
 */
const {addPlanService,getAllPlanService,getTodayPlanService,deletePlanService,updatePlanService, searchPlanService} = require("../service/plan")

async function addPlanController(req,res){
    let {content,level,noteTip,completeTime} = req.body;
    let userId = req.session.userInfo._id;
    let plan = {userId,content,level,noteTip,completeTime};
    plan.completeTime = 28800000 + plan.completeTime*1;
    plan.completeTime = plan.completeTime + '';
    let result = await addPlanService(plan);
    if(result !== '01'){
        res.send({
            code: '0000',
            msg: "计划添加成功"
        });
    } else res.send({
            code: '0001',
            msg: "计划添加失败，请稍候重试"
        });
}

async function getAllPlanController(req,res){
    let userId = req.session.userInfo._id;
    console.log('用户ID',userId);
    if(!userId){
        res.send({
            code: '0001',
            msg: "获取计划列表失败，请确认用户Id"
        });
        return;
    }
    let pageMsg = {
        userId,
        currentPage:req.query.currentPage || 1,
        pageSize:req.query.pageSize || 10
    };
    console.log(pageMsg)
    let result = await getAllPlanService(pageMsg);
    res.send({
        code: '0000',
        msg: '数据获取成功',
        data: result
    })
}

async function getTodayPlanController (req, res) {
    let userId = req.session.userInfo._id;
    console.log('用户ID',userId);
    if(!userId){
        res.send({
            code: '0001',
            msg: "获取计划列表失败，请确认用户Id"
        });
        return;
    }
    const result = await getTodayPlanService(userId)
    res.send({
        code: '0000',
        msg: '数据获取成功',
        data: result
    })
}


async function deletePlanController(req,res){
    let planId = req.body.planId;
    console.log('计划ID',planId);
    if(!planId){
        res.send({
            code: '0001',
            msg: "删除计划失败，请确认计划Id"
        });
        return;
    }
    let result = await deletePlanService(planId);
    if(result === '01'){
        res.send({
            code: '0002',
            msg: "未查询到该计划数据，请确认"
        });
        return;
    };
    res.send({
        code: '0000',
        msg: "删除计划成功"
    });
}

async function updatePlanController(req,res){
    let planId = req.body.planId;
    console.log('计划ID',planId);
    if(!planId){
        res.send({
            code: '0001',
            msg: "更新计划失败，请确认计划Id"
        });
        return;
    };
    let result;
    if(req.body.state){
        let plan = {state: req.body.state};
        result = await updatePlanService(planId,plan);
    }

    else {
        let {content,level,noteTip,completeTime} = req.body;
        let plan = {content,level,noteTip,completeTime};
        plan.completeTime = 28800000 + plan.completeTime*1;
        plan.completeTime = plan.completeTime + '';
        result = await updatePlanService(planId,plan);
    }
    if(result === '01'){
        res.send({
            code: '0002',
            msg: "未查询到该计划数据，请确认"
        });
        return;
    }
    res.send({
        code: '0000',
        msg: "更新计划成功"
    });
}

async function searchPlanController (req, res) {
    const { currentPage, pageSize} = req.body
    let paramsObj = {currentPage, pageSize}
    req.body.searchWords && Object.assign(paramsObj, {searchWords: req.body.searchWords})
    req.body.level && Object.assign(paramsObj, {leval: req.body.level})
    req.body.state && Object.assign(paramsObj, {state: req.body.state})
    const result =  await searchPlanService(paramsObj)
    res.send({
        code: '0000',
        msg: '数据获取成功',
        data: result
    })
}

module.exports = {
    addPlanController,
    getAllPlanController,
    deletePlanController,
    updatePlanController,
    searchPlanController,
    getTodayPlanController
}