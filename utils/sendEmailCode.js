/*
 * @Descripttion: 发送邮箱验证码
 * @version: 1.0.0
 * @Author: xuecheng
 * @Date: 2022-03-31 14:47:49
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-04-01 18:10:41
 */


const NodeMailer = require('nodemailer')
const {EMAILNAME,PASS} = require("../config/index")

const maileServe = NodeMailer.createTransport({
    service: 'qq',
    auth: {
        user: EMAILNAME,
        pass: PASS
    }
})

exports.sendEmailCodes = async function(email,code){
    let options = {
        from: `成叔<1781367484@qq.com>`,
        subject: `NoteBook 注册验证码`,
        to: email,
        html: `<div style="text-align: center;">
                    <h2>欢迎使用NoteBook</h2>
                    <h3>您的邮箱验证码是：<span style="color: #e60027;font-size: 28px;font-weight: bold;">${code}</span></h3>
                    <h3>此验证码3分钟内有效，请及时查看。</h3>
                    <p>-----------------------------------------------</p>
                    <h5>技术由私人代理服务器提供，感谢您的使用！</h5>
                </div>`
    }

    await maileServe.sendMail(options,(err,msg) => {
        if(!err){
            return "00"
        }
        else return "01"
    })
}
