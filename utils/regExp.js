/*
 * @Descripttion: 格式校验工具
 * @version: 1.0.0
 * @Author: xuecheng
 * @Date: 2022-04-01 16:00:08
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-04-07 16:46:38
 */

var emailReg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;



const isEmail = (email) => {
    return emailReg.test(email);
}



module.exports = {
    isEmail
}