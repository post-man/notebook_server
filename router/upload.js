/*
 * @Descripttion: 上传路由
 * @version: 1.0.0
 * @Author: xuecheng
 * @Date: 2022-03-25 14:11:02
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-04-08 11:27:24
 */

const multer = require('multer')            //接收请求文件
const uploadRouter = require('express').Router();
const path = require('path')
const {connect} = require("../db");
const { updateMsgController }  = require("../controller/user")

const storage = multer.diskStorage({
    destination: (req,file,cb) => {
        cb(null,'static/images/avatars/');     //路径
    },
    filename: (req,file,cb) => {
        const ext = path.extname(file.originalname);
        const fileName = `avatar_${new Date().getTime()}_${Math.random().toString().slice(2,8)}${ext}`;
        cb(null, fileName);        //名字
    }
}) 

const imgUpload = multer({storage: storage})

const handleAvatar = (req, res, next) => {
    const avatar = req.file.path.replace('static','')
    const userId = req.session.userInfo._id
    console.log('头像上传', avatar.replace('static',''))
    console.log('用户id', userId)
    req.body.avatar = avatar
    next()
}


uploadRouter.post("/image", imgUpload.single('avatar'), handleAvatar, updateMsgController)


module.exports = uploadRouter