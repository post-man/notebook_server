/*
 * @Descripttion: 笔记模块
 * @version: 1.0.0
 * @Author: xuecheng
 * @Date: 2022-03-25 10:41:24
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-04-08 11:28:33
 */

const noteRouter = require('express').Router()
const { isLogin } = require('../middleWare/authCommon')
const { getNotesController, addNotesController, deleteNotesController, updateNotesController } = require('../controller/note')

noteRouter.get("/", isLogin, getNotesController)

noteRouter.post("/addNote", isLogin, addNotesController)

noteRouter.post("/updateNote", isLogin, updateNotesController)

noteRouter.post("/deleteNote", isLogin, deleteNotesController)

module.exports = noteRouter

