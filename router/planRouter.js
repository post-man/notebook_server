/*
 * @Descripttion: 计划模块
 * @version: 1.0.0
 * @Author: xuecheng
 * @Date: 2022-04-08 11:28:52
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-04-19 16:38:31
 */

const planRouter = require('express').Router()
const {addPlanController,getAllPlanController,getTodayPlanController,deletePlanController,updatePlanController, searchPlanController} = require("../controller/plan")
const {isLogin} = require('../middleWare/authCommon')
const {validateAddPlan} = require('../middleWare/validatePramas')

planRouter.get('/',isLogin,getAllPlanController)

// 首页获取当天的计划列表
planRouter.get('/getTodayPlan',isLogin,getTodayPlanController)

planRouter.post('/addPlan',isLogin, validateAddPlan, addPlanController)

planRouter.post('/deletePlan',isLogin, deletePlanController)

planRouter.post('/updatePlan',isLogin, updatePlanController)

planRouter.post('/getPlansByOptions',isLogin, searchPlanController)


module.exports = planRouter
