/**
 * 完成计划  未完成计划模块
 * */

const subPlanRouter = require('express').Router()
const { isLogin } = require('../middleWare/authCommon')
const { getPlanAchivedController, getPlanNoAchivedController, editPlanController } = require('../controller/subPlan')

// 已完成计划
subPlanRouter.get('/planAchived', isLogin, getPlanAchivedController)

// 未完成计划
subPlanRouter.get('/planNoAchive', isLogin, getPlanNoAchivedController)

//重新编辑计划
subPlanRouter.post('/editPlan', isLogin, editPlanController)


module.exports = subPlanRouter