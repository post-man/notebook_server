/*
 * @Descripttion: 登录模块
 * @version: 1.0.0
 * @Author: xuecheng
 * @Date: 2022-03-25 10:41:14
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-04-07 16:49:52
 */
const authRouter = require('express').Router();
const {checkUserName,registerController,loginController,sendEmailCode} = require("../controller/auth")
const {validateRegister,validateEmail,validateUserName,validateLogin} = require("../middleWare/validatePramas")
const {encryptionPassowrd} = require("../middleWare/authCommon")

authRouter.get('/', (req, res) => {res.send("欢迎访问烂笔头网址！")})

authRouter.get('/checkUserName',validateUserName,checkUserName)

authRouter.get('/sendEmailCode',validateEmail,sendEmailCode)

authRouter.post('/register', validateRegister,encryptionPassowrd,registerController)

authRouter.post('/login',validateLogin,encryptionPassowrd,loginController)


module.exports = authRouter
