/*
 * @Descripttion: 用户模块
 * @version: 1.0.0
 * @Author: xuecheng
 * @Date: 2022-03-25 10:41:24
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-04-08 11:28:33
 */

const userRouter = require('express').Router();
const { isLogin, encryptionPassowrd } = require('../middleWare/authCommon')
const { getUserMsgController, updateMsgController, resetPasswordController } = require('../controller/user')

userRouter.get("/", isLogin, getUserMsgController)

userRouter.post('/updateMsg', isLogin, updateMsgController)

userRouter.post('/resetPassWord', isLogin, encryptionPassowrd, resetPasswordController)


module.exports = userRouter