/*
 * @Descripttion: 接口路由
 * @version: 1.0.0
 * @Author: xuecheng
 * @Date: 2022-03-25 10:40:41
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-04-08 11:38:43
 */
const authRouter = require("./authRouter")
const userRouter = require("./userRouter")
const uploadRouter = require('./upload')
const planRouter = require('./planRouter')
const subPlanRouter = require('./subPlanRouter')
const noteRouter = require("./noteRouter")

module.exports = app => {

    app.use(authRouter);

    app.use('/plan',planRouter);

    app.use('/subplan',subPlanRouter);

    app.use('/user',userRouter);

    app.use('/upload',uploadRouter);

    app.use('/note',noteRouter);


    //全局错误处理中间件
    app.use(function(err,req, res, next) {
        if(err){
            res.status(502);
        } else res.status(404);
        res.send({
            code: err ? '502' : '404',
            message:'请求错误',
            err:err
        });
    })
}
